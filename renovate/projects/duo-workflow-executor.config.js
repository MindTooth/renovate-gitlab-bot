const {
  createServerConfig,
  baseConfig,
  defaultLabels,
  availableRouletteReviewerByRole,
} = require("../lib/shared");
const { updateDangerReviewComponent } = require("../lib/components");

module.exports = createServerConfig([
  {
    repository: "gitlab-renovate-forks/duo-workflow-executor",
    ...baseConfig,
    labels: [...defaultLabels, "golang", "Category:Duo Workflow"],
    reviewers: availableRouletteReviewerByRole("duo-workflow-executor"),
    reviewersSampleSize: 1,
    prConcurrentLimit: 4,
    enabledManagers: ["asdf", "gomod", "custom.regex"],
    semanticCommits: "disabled",
    packageRules: [],
    postUpdateOptions: ["gomodTidy", "gomodUpdateImportPaths"],
    ...updateDangerReviewComponent,
  },
]);
